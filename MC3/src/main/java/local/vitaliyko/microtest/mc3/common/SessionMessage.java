package local.vitaliyko.microtest.mc3.common;

import lombok.NoArgsConstructor;

import java.util.Date;


@NoArgsConstructor
public class SessionMessage {

    private Integer id;
    private Integer session_id;

    private Date MC1_timestamp;
    private Date MC2_timestamp;
    private Date MC3_timestamp;
    private Date end_timestamp;

}
