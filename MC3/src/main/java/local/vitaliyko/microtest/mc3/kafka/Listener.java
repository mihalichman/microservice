package local.vitaliyko.microtest.mc3.kafka;

import local.vitaliyko.microtest.mc3.common.SessionMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;

public class Listener {
//    @Autowired
//    private DataService dataService;

    @KafkaListener(topics = "${kafka.topic.name}", containerFactory = "kafkaListenerContainerFactory")
    public void listener(SessionMessage data) {
        System.out.println("Recieved message: " + data);
        // dataService.saveMessage(data);
    }
}
