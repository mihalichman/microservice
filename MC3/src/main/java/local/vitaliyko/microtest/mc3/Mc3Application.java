package local.vitaliyko.microtest.mc3;

import local.vitaliyko.microtest.mc3.kafka.Listener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.listener.MessageListener;

@SpringBootApplication
public class Mc3Application {

    public static void main(String[] args) {
        SpringApplication.run(Mc3Application.class, args);
    }

    @Bean
    public Listener messageListener() {
        return new Listener();
    }
}
