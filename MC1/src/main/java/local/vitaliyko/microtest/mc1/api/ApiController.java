package local.vitaliyko.microtest.mc1.api;

import local.vitaliyko.microtest.mc1.common.SessionMessage;
import local.vitaliyko.microtest.mc1.socket.WebSocketSessionHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ApiController {

    private final Logger logger = LogManager.getLogger(WebSocketSessionHandler.class);

    // period of time working
    @Value("${application.time.value}")
    private String applicationTime;

    // database url
    @Value("${spring.datasource.url}")
    private String dataUrl;

    private boolean IN_PROGRESS = false;

    @Autowired
    MessageServiceIF messageService = new MessageService();

    /**
     * Health checker
     * */
    @RequestMapping(value="/healthcheck", method= RequestMethod.GET)
    public ResponseEntity<String> healthcheck() {
        return new ResponseEntity<>("ok", HttpStatus.OK);
    }

    /**
     * Start method, begin sending message
     * */
    @RequestMapping(value="/start", method = RequestMethod.GET)
    public ResponseEntity<String> start() {
        logger.info("application : {}", applicationTime);
        logger.info("db url : {}", dataUrl);

        if (IN_PROGRESS){
            return new ResponseEntity<>("loop now in progress, if you want start new, stop previous and run again ", HttpStatus.NOT_MODIFIED);
        }else {
            IN_PROGRESS = true;
            begin();
            return new ResponseEntity<>(applicationTime, HttpStatus.OK);
        }
    }

    /**
     * Receive message from MC3
     * */
    @RequestMapping(value="/stop", method = RequestMethod.GET)
    public ResponseEntity<String> stop() {
        // TODO stop loop message, use stop global
        IN_PROGRESS = false;
        return ResponseEntity.ok().build();
    }

    /**
     * Receive message from MC3
     * */
    @RequestMapping(value="/message", method = RequestMethod.POST)
    public ResponseEntity<String> message() {
        // TODO get message form MC3

        begin();

        return ResponseEntity.ok().build();
    }

    /**
     * Get status about system (work time, count, worker status)
     * */
    @RequestMapping(value="/status", method = RequestMethod.GET)
    public ResponseEntity<String> status() {
        // TODO get status, get data from DB ()
        return ResponseEntity.ok().build();
    }

    /**
     * Begin or continue sending loop
     * */
    private void begin(){
        //TODO add check about worktime time
        if (IN_PROGRESS){
            SessionMessage message = messageService.registerMessage();
            messageService.sendMessageWS(message);
        }
    }
}
