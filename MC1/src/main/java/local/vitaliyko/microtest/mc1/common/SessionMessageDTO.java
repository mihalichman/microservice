package local.vitaliyko.microtest.mc1.common;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="message")
public class SessionMessageDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name="session_id")
    private Integer session_id;

    @Column(name="mc1_timestamp")
    private Date MC1_timestamp;

    @Column(name="mc2_timestamp")
    private Date MC2_timestamp;

    @Column(name="mc3_timestamp")
    private Date MC3_timestamp;

    @Column(name="end_timestamp")
    private Date end_timestamp;

    public void setMC1_timestamp(Date mc1_timestamp) {
        this.MC1_timestamp = mc1_timestamp;
    }

    protected SessionMessageDTO() {}

    public SessionMessageDTO(Integer session_id, Date mc1_timestamp) {
        this.session_id = session_id;
        this.MC1_timestamp = mc1_timestamp;
    }

    public String toString() {
        return String.format("id=%d, session_id='%s'",
                id, session_id);
    }
}
