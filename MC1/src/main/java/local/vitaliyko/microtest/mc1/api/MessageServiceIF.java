package local.vitaliyko.microtest.mc1.api;

import local.vitaliyko.microtest.mc1.common.SessionMessage;

import java.util.Collection;

public interface MessageServiceIF {

    /**
     * Save message to DB if they have full data
     * */
    void saveMessage();

    /**
     * Check message right content
     * */
    void checkMessage();

    /**
     * Register message,
     * */
    SessionMessage registerMessage();

    /**
     * Send message to WS
     * */
    void sendMessageWS(SessionMessage message);

    /**
     * Find messages,
     * */
    Collection<SessionMessage> findMessages();
}
