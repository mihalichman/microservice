package local.vitaliyko.microtest.mc1.api;

import local.vitaliyko.microtest.mc1.common.SessionMessage;
import local.vitaliyko.microtest.mc1.socket.WebSocketSessionHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;


/**
 * Class worker with message (register, save, check etc)
 * */
@Service
public class MessageService implements MessageServiceIF{

    private final Logger logger = LogManager.getLogger(WebSocketSessionHandler.class);

    @Value("${application.websocket.server.host}")
    private String websocketHost;
    @Value("${application.websocket.server.port}")
    private String websocketPort;
    @Value("${application.websocket.server.path}")
    private String websocketPath;

    @Override
    public void saveMessage() {
        // TODO save message result to DB
    }

    @Override
    public void checkMessage() {
        // TODO check message right session_id
    }

    @Override
    public SessionMessage registerMessage() {
        //TODO DB manipulation (create session)

        // register message, set mc1 timestamp
        SessionMessage message = new SessionMessage();
        message.setMC1_timestamp(new Date());

        return message;
    }

    @Override
    public void sendMessageWS(SessionMessage message) {
        String URL = "ws://" + websocketHost + ":" + websocketPort + websocketPath;

        logger.info("ws host : {}, {}", URL, websocketHost);

        try {

            WebSocketClient client = new StandardWebSocketClient();

            WebSocketStompClient stompClient = new WebSocketStompClient(client);
            stompClient.setMessageConverter(new MappingJackson2MessageConverter());
            StompSessionHandler sessionHandler = new WebSocketSessionHandler();
            StompSession session = stompClient.connect(URL, sessionHandler).get();

            session.send("/message", message);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        new Scanner(System.in).nextLine();

    }

    @Override
    public Collection<SessionMessage> findMessages() {
        return new ArrayList<>();
    }
}
