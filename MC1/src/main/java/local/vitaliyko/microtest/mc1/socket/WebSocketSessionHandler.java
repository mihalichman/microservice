package local.vitaliyko.microtest.mc1.socket;

import local.vitaliyko.microtest.mc1.common.SessionMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

public class WebSocketSessionHandler extends StompSessionHandlerAdapter {

    private final Logger logger = LogManager.getLogger(WebSocketSessionHandler.class);

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {

        logger.info("New session established : " + session.getSessionId());

        session.subscribe("/topic/messages", this);
        logger.info("Subscribed to /topic/messages");

        session.send("/app/message", new SessionMessage());
        logger.info("Message sent to websocket server");
    }
    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        Message msg = (Message) payload;
        logger.info("Received : " + msg.getPayload()+ " from : " + msg.getHeaders());
    }

    @Override
    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        logger.error("Got an exception", exception);
    }

}
