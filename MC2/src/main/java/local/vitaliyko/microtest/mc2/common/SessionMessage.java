package local.vitaliyko.microtest.mc2.common;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@NoArgsConstructor
@Getter @Setter
public class SessionMessage {

    private Integer id;
    private Integer session_id;

    private Date MC1_timestamp;
    private Date MC2_timestamp;
    private Date MC3_timestamp;
    private Date end_timestamp;

}
