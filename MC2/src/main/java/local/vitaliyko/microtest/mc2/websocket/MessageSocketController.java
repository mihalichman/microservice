package local.vitaliyko.microtest.mc2.websocket;

import local.vitaliyko.microtest.mc2.common.SessionMessage;
import local.vitaliyko.microtest.mc2.kafka.MessageProducer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

import java.util.Date;

@Controller
public class MessageSocketController {

    private final Logger logger = LogManager.getLogger(MessageSocketController.class);

    @Autowired
    private MessageProducer messageProducer;

    @MessageMapping("/message")
    @SendTo("/topic/messages")
    public String greeting(SessionMessage message) throws Exception {
        Thread.sleep(1000); // simulated delay

        logger.info(" Recieved message: {}", message);

        message.setMC2_timestamp(new Date());
        messageProducer.sendMessage(message);

        return "Hello, " + HtmlUtils.htmlEscape(message.toString()) + "!";
    }

}
